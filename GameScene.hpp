#pragma once
#include <QGraphicsScene>
#include <QErrorMessage>
#include <memory>
#include <vector>

#include "GameTile.hpp"
#include "main.hpp"

template<typename T>
using Matrix = std::vector<std::vector<T>>;
constexpr QPoint positionNone(-1, -1);



class GameScene: public QGraphicsScene
{
    Q_OBJECT
    public:
        GameScene(QObject* parent=nullptr);
    public slots:
        void startGame();
        void loadImage();
        void clickedOnTile(Qt::MouseButton button, QPoint position);
        void showError();
        void showWin();
    signals:
        void noImageLoaded();
        void win();
    private:
        void cutPieces();
        void checkSwap();
        bool checkWin();
        void paintTileBorder(QPoint position, TileColor color);
        std::unique_ptr<QPixmap> m_image;
        Matrix<std::unique_ptr<GameTile>> m_pieces;
        QPoint lkm_selected {positionNone};
        QPoint pkm_selected {positionNone};
        GameState m_state;
        QErrorMessage errorDial = QErrorMessage(mainWindow);
        std::unique_ptr<QGraphicsPixmapItem> m_winItem;        
        std::unique_ptr<QPixmap> m_winPixmap;        
};