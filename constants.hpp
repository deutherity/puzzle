#pragma once
#include <QRectF>
#include <QPoint>
#include <cstdint>
constexpr int borderWidth = 3;
constexpr int tiles_width_count = 4;
constexpr int tiles_height_count = 4;


enum TileColor: std::uint32_t
{
    // AARRGGBB (QRgb format)
    Black  = 0xff000000,
    Orange = 0xffff9632,
    Blue   = 0xff26578e
};

struct GameState
{
    QRectF sceneRect;
    QPoint tile_count;
};