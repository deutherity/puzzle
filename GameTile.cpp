#include <QColor>
#include <QPainter>

#include "GameTile.hpp"
#include "midpoint.hpp"
#include "constants.hpp"


GameTile::GameTile(QPoint realPosition, const GameState* state, const QPixmap& icon, QGraphicsItem* parent):
    m_original(icon), m_realPosition(realPosition), m_state(state)
{
    setBorderColor(Black);
    setShapeMode(BoundingRectShape);
}

bool GameTile::correctPosition() const
{
    return m_realPosition == m_position;
}

void GameTile::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    emit clicked(event->button(), m_position);
}

void GameTile::setPosition(QPoint pos)
{
    m_position = pos;
    const QRectF& sceneRect = m_state->sceneRect;
    const QPoint& tc = m_state->tile_count;
    auto x = midpoint(pos.rx(), tc.x(), sceneRect.width());
    auto y = midpoint(pos.ry(), tc.y(), sceneRect.height());
    setPos(QPointF(x, y));
}

void GameTile::setBorderColor(TileColor color)
{
    QColor borderColor(static_cast<QRgb>(color));
    QPixmap copy_original = m_original;
    QPainter painter(&copy_original);
    painter.setPen(QPen(borderColor, borderWidth));
    constexpr int dw = borderWidth - 1;
    auto && border = copy_original.rect().adjusted(dw,  dw,
                                                  -dw, -dw);
    painter.drawRect(border);
    setPixmap(copy_original);
}
