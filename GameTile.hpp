#pragma once

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QPixmap>
#include <QPoint>

#include "constants.hpp"

class GameTile: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    public:
        GameTile(QPoint realPosition, const GameState* state, const QPixmap& icon, QGraphicsItem* parent=nullptr);
        
        void mousePressEvent(QGraphicsSceneMouseEvent *event) final;
        bool correctPosition() const;
    signals:
        void clicked(Qt::MouseButton button, QPoint position);
    public slots:
        void setBorderColor(TileColor color);
        void setPosition(QPoint position);
    private:
        const QPixmap m_original;
        QPoint m_position;
        const QPoint m_realPosition;
        const GameState *m_state;
};