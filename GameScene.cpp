#include <QErrorMessage>
#include <QFileDialog>
#include <QPainter>

#include <concepts>
#include <algorithm>
#include <random>
#include <memory>
#include <utility>

#include "GameScene.hpp"
#include "midpoint.hpp"


void GameScene::cutPieces()
{
    Matrix<std::unique_ptr<GameTile>>& pieces = m_pieces;
    const QPixmap& image = *m_image;
    const auto& tiles_count = m_state.tile_count;
    int x = tiles_count.x();
    int y = tiles_count.y();
    const qreal total_width  = static_cast<qreal>(image.width ());
    const qreal total_height = static_cast<qreal>(image.height());
    const qreal piece_width  = total_width  / static_cast<qreal>(x);
    const qreal piece_height = total_height / static_cast<qreal>(y);
    const QSizeF piece_sizef(piece_width, piece_height);

    std::vector<std::unique_ptr<GameTile>> unshuffled;

    for (int j = 0; j < y; ++j)
    for (int i = 0; i < x; ++i)
    {
        const QPointF topLeftf(
            midpoint(i, x, total_width ),
            midpoint(j, y, total_height)
            );
        const QRectF rectf(topLeftf, piece_sizef);
        const QPoint realPosition (i, j);
        unshuffled.push_back(std::make_unique<GameTile>(realPosition, &m_state, image.copy(rectf.toRect())));
    }
    
    std::random_device device;
    std::mt19937 g(device());
    std::shuffle(unshuffled.begin(), unshuffled.end(), g);

    pieces.clear();
    pieces.reserve(y);
    for (int j = 0; j < y; ++j)
    {
        pieces.push_back({});
        pieces[j].reserve(x);
        for (int i = 0; i < x; ++i)
        {
            pieces[j].push_back(std::move(unshuffled.back()));
            unshuffled.pop_back();
            pieces[j][i]->setPosition({i, j});
        }
    }

}

void GameScene::showError()
{
    errorDial.showMessage("No image selected");
}

GameScene::GameScene(QObject* parent):
    QGraphicsScene(parent)
{
    connect(this, &GameScene::noImageLoaded, this, &GameScene::showError);
    connect(this, &GameScene::win, this, &GameScene::showWin);
}

void GameScene::loadImage()
{
    static QString filter = "Images (*.png *.bmp *.jpg *.jpeg)";
    QFileDialog dialog(mainWindow, "Select an image", QDir::currentPath(), filter);
    dialog.setFileMode(QFileDialog::ExistingFile);
    if (dialog.exec())
    {
        const QString filename = dialog.selectedFiles().at(0);
        m_image = std::make_unique<QPixmap>(filename);
    }
}

void GameScene::startGame()
{
    if (!m_image)
    {
        emit noImageLoaded();
        return;
    }

    /*
    if (m_winItem)
        m_winItem.reset(nullptr);
    */

    const QRect& imageRect = m_image->rect();
    QRectF imageRectf(0,0, imageRect.width(), imageRect.height());
    setSceneRect(imageRectf);
    m_state.sceneRect = imageRectf;
    m_state.tile_count = QPoint(tiles_width_count, tiles_height_count);
    cutPieces();
    for (int i = 0; i < m_state.tile_count.x(); ++i)
    for (int j = 0; j < m_state.tile_count.y(); ++j)
    {
        auto item = m_pieces[j][i].get();
        connect(item, &GameTile::clicked, this, &GameScene::clickedOnTile);
        addItem(item);
    }
}

void GameScene::clickedOnTile(Qt::MouseButton button, QPoint position)
{
    QPoint* primary;
    QPoint* secondary;
    TileColor primaryColor;
    switch (button)
    {
    case Qt::LeftButton:
        primary = &lkm_selected;
        secondary = &pkm_selected;
        primaryColor = Blue;
        break;
    case Qt::RightButton:
        primary = &pkm_selected;
        secondary = &lkm_selected;
        primaryColor = Orange;
        break;
    }
    if (*secondary == position)
    {
        *secondary = positionNone;
        *primary = position;
        paintTileBorder(position, primaryColor);
    }
    else if (*primary == positionNone)
    {
        *primary = position;
        paintTileBorder(position, primaryColor);
    }
    else if (*primary == position)
    {
        *primary = positionNone;
        paintTileBorder(position, Black);
    }
    else
    {
        paintTileBorder(*primary, Black);
        *primary = position;
        paintTileBorder(*primary, primaryColor);
    }
    checkSwap();
}

void GameScene::paintTileBorder(QPoint pos, TileColor color)
{
    m_pieces[pos.ry()][pos.rx()]->setBorderColor(color);
}

void GameScene::checkSwap()
{
    if (lkm_selected != positionNone &&
        pkm_selected != positionNone &&
        pkm_selected != lkm_selected)
    {
        paintTileBorder(lkm_selected, Black);
        paintTileBorder(pkm_selected, Black);
        m_pieces[pkm_selected.ry()][pkm_selected.rx()]->setPosition(lkm_selected);
        m_pieces[lkm_selected.ry()][lkm_selected.rx()]->setPosition(pkm_selected);
        std::swap(m_pieces[pkm_selected.ry()][pkm_selected.rx()],
                  m_pieces[lkm_selected.ry()][lkm_selected.rx()]);
        pkm_selected = positionNone;
        lkm_selected = positionNone;
        if (checkWin())
            emit win();
    }
}

bool GameScene::checkWin()
{
    for (int j = 0; j < m_state.tile_count.y(); ++j)
    for (int i = 0; i < m_state.tile_count.x(); ++i)
        if (!(m_pieces[j][i]->correctPosition()))
            return false;
    return true;
}

void GameScene::showWin()
{
    m_pieces.clear();
    m_winPixmap = std::make_unique<QPixmap>(*m_image);
    QPainter painter(m_winPixmap.get());

    
    QColor borderColor(static_cast<QRgb>(Black));
    painter.setPen(QPen(borderColor, borderWidth));
    constexpr int dw = borderWidth;
    auto && border = m_image->rect().adjusted(dw,  dw,
                                             -dw, -dw);
    painter.drawRect(border);

    m_winItem = std::make_unique<QGraphicsPixmapItem>(*m_winPixmap);
    
    addItem(m_winItem.get());
}
