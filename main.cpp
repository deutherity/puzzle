#include <QApplication>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPushButton>

#include <cstdlib>

#include "main.hpp"
#include "GameScene.hpp"


QWidget* mainWindow;


int main(int argc, char ** argv)
{

	QApplication app(argc, argv);
	QWidget window;
	mainWindow = &window;
	QVBoxLayout vbox(&window);
	QHBoxLayout hboxButtons;


	GameScene board;
	QGraphicsView boardWidget(&board);
	vbox.addWidget(&boardWidget);
	vbox.addLayout(&hboxButtons);
	

	QPushButton btnReset("play");
	QPushButton btnLoad("load");
	QPushButton btnExit("exit");

	hboxButtons.addWidget(&btnReset);
	hboxButtons.addWidget(&btnLoad);
	hboxButtons.addWidget(&btnExit);

  	QObject::connect(&btnReset, &QPushButton::clicked, &board, &GameScene::startGame);
  	QObject::connect(&btnLoad,  &QPushButton::clicked, &board, &GameScene::loadImage);
 	QObject::connect(&btnExit,  &QPushButton::clicked, &window, &QWidget::close);

	window.show();
	return app.exec();
}
