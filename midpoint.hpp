#pragma once
#include <concepts>

template<std::floating_point T>
T midpoint(int at, int total, T length)
{
    return static_cast<T>(at) * length / static_cast<T>(total);
}